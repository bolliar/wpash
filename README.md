# wpa

Simple sh script to work with wpa_supplicant.

# Dependencies

* wpa_supplicant
* iw
* dhcpcd

# HELP PAGE

```
scan[--scan|-s]: Scan for available wireless networks around you.

add[--add|-a]: Add a network.

connect[--connect|-c]: Connect to a network.

disconnect[--disconnect|-d]: Disconnect from the current network.

forget[--forget|-f]: Remove a network.

help[--help|-h]: Display this help message.
```
