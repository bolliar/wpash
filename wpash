#!/bin/sh

# Define variables with default values
NUM=""
PASS=""
CARD=""

# Get wireless interface card name
get_card() {
  CARD=$(iw dev | awk '/Interface/ {print $2}')
}

# Scan for available wireless networks around you
scan() {
  iw dev "$CARD" scan | awk -F: '/SSID:/ {print $2}'
}

# Connect to a network
connect() {
  wpa_supplicant -B -i "$CARD" -c "/etc/wpa_supplicant/$NUM.conf" && dhclient -nw
  if [ $? -eq 0 ]; then
    echo "Successfully connected!" 
    cp "/etc/wpa_supplicant/$NUM.conf" /etc/wpa_supplicant/wpa_supplicant.conf
  else
    echo "Connection failed!" 
  fi
}

# Disconnect from the current network
disconnect() {
  killall wpa_supplicant && dhclient -r && rm /etc/wpa_supplicant/wpa_supplicant.conf
  echo "Disconnected!"
}

# Add a network
add() {
  if [ ${#PASS} -lt 8 ]; then
    echo "The password must be at least 8 characters"
  else
    touch "/etc/wpa_supplicant/$NUM.conf"
    echo "ctrl_interface=/run/wpa_supplicant" >> "/etc/wpa_supplicant/$NUM.conf"
    echo "update_config=1" >> "/etc/wpa_supplicant/$NUM.conf"
    wpa_passphrase "$NUM" "$PASS" >> "/etc/wpa_supplicant/$NUM.conf"
    if grep -q "psk=" "/etc/wpa_supplicant/$NUM.conf"; then
      echo "Network added successfully"
    else
      echo "Failed to add network"
      rm "/etc/wpa_supplicant/$NUM.conf"
    fi
  fi
}

# Remove a network
forget() {
  rm -f "/etc/wpa_supplicant/$NUM.conf" "/etc/wpa_supplicant/wpa_supplicant.conf"
  killall wpa_supplicant && dhclient -r
  echo "Deleted!"
}

# Display help information
help() {
  echo "Usage: wpash [OPTION]"
  echo "Scan for available wireless networks around you."
  echo "  --scan, -s"
  echo
  echo "Add a network."
  echo "  --add, -a <ssid> <password>"
  echo
  echo "Connect to a network."
  echo "  --connect, -c <ssid>"
  echo
  echo "Disconnect from the current network."
  echo "  --disconnect, -d"
  echo
  echo "Remove a network."
  echo "  --forget, -f <ssid>"
  echo
  echo "Display this help message."
  echo "  --help, -h"
}


# Main program
get_card

# Parse command-line options
OPTS=$(getopt --options=s,a:,c:,d,f:,h --longoptions=scan,add:,connect:,disconnect,forget:,help --name "$0" -- "$@")
if [ $? -ne 0 ]; then
  echo "Failed to parse command-line options"
  exit 1
fi
eval set -- "$OPTS"

while true; do
  case "$1" in
    --scan|-s)
      scan
      shift
      ;;
    --add|-a)
      NUM="$2"
      PASS="$3"
      add
      shift 3
      ;;
    --connect|-c)
      NUM="$2"
      connect
      shift 2
      ;;
    --disconnect|-d)
      disconnect
      shift
      ;;
    --forget|-f)
      NUM="$2"
      forget
      shift 2
      ;;
    --help|-h)
      help
      shift
      ;;
    --)
      shift
      break
      ;;
    *)
      echo "Invalid option: $1"
      echo "Try 'wpash --help' for more information."
      exit 1
      ;;
  esac
done
